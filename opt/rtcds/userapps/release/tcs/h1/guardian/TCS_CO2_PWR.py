from guardian import GuardState, GuardStateDecorator, NodeManager
from lscparams import tcs_nom_annular_pwr
import time
import numpy as np

##################################################

request = 'NO_OUTPUT'
nominal = 'NOM_ANNULAR_POWER'

# Arm specifc constants
if _SYSTEM_ == 'TCS_ITMX_CO2_PWR':
    mask_node = 'TCS_CO2X_MASKS'
    nom_annular_pwr = tcs_nom_annular_pwr['X']
    arm = 'X'
elif _SYSTEM_ == 'TCS_ITMY_CO2_PWR':
    mask_node = 'TCS_CO2Y_MASKS'
    nom_annular_pwr = tcs_nom_annular_pwr['Y']
    arm = 'Y'
else:
    raise IOError('Couldn\'t parse _SYSTEM_ to determine subbordinate mask node')

nodes = NodeManager([mask_node])

PRG = lambda P_in: np.interp(P_in,
                             [2, 20, 25, 30, 50],  # Input powers
                             [44, 47, 44, 40, 35])  # PRG we actually achieve

# What's the target diopter? Is it 1/34.5km or 1/50km?? (In substrate or out of substrate lens)
Dtarget = 20e-6
co2x_D_W = 15e-6
co2y_D_W = 25e-6
RH_D_W = 9e-6
SH_D_W = 4.9e-4
ITMY_ABS = 800e-9
ITMX_ABS = 300e-9
arm_gain = 275
P_arm = lambda P_in: PRG(P_in) * arm_gain * P_in / 2

# Should probably store and grab these numbers from the simulation page
# All these are single pass lens calculations
Dx = lambda P, co2, RH: P_arm(P) * ITMX_ABS * SH_D_W + co2x_D_W * co2 - RH_D_W * RH
Dy = lambda P, co2, RH: P_arm(P) * ITMY_ABS * SH_D_W + co2y_D_W * co2 - RH_D_W * RH

"""
The guardian will linearly interpolate the CO2 powers
between each of the values in this dictionary. The key
being the power and the value the Watts to set.
The interpolator will continue the last or first value
if the PSL input goes out of the range specified here.

This RH factor of two is the total power of the upper and lower
ring heaters
"""

# Changing settings from 2W for ITMX, 1W for ITMY @2WPSL, 
# 0.7W @20WPSL, 0W @50WPSL to all be zero until values are tested. CMC 2022/04/25

CEN_CO2_POWER_INTERP = {
    "TCS_ITMX_CO2_PWR": np.array([
        (2, 0),
        (20, 0),
        (50, 0),
    ]),
    "TCS_ITMY_CO2_PWR": np.array([
        (2, 0),
        (20, 0),
        (50, 0),
    ]),
}

CEN_CO2_POWER = CEN_CO2_POWER_INTERP[_SYSTEM_]
# This is the +/- W value from what the current CO2 pwr is,
# before it will update and change the CO2 power
CO2_RANGE = 0.1

##################################################
# Functions and decorators
##################################################

'''
There are two BUSY channels:
$(IFO):TCS-ITM{}_CO2_LASERPOWER_STATE_BUSY
$(IFO):TCS-ITM{}_CO2_LASERPOWER_RS_BUSY

The two channels follow each other for the most part,
BUT the RS_BUSY channel stays in the busy (1) state for
one second longer.

The power is still changing for ~0.2s after the
STATE_BUSY channel has gone back to zero, so
RS_BUSY is delayed by ~0.8s. This still seems
like the safer option.

alog67399 now using both channels as above is not always true.
'''


def is_busy():
    state1 = ezca['LASERPOWER_RS_BUSY']
    state2 = ezca['LASERPOWER_STATE_BUSY']
    if state1 == 1 or state2 == 1:
        return True
    else:
        return False


def in_fault():
    flag = False
    if ezca['LASERONOFFSWITCH'] == 0:
        log('The laser is not on!')
        notify('The laser is not on!')
        flag = True
    elif ezca['INTRLK_RTD_OR_IR_ALRM'] != 0:
        log('RTD/IR ALARM TRIPPED.')
        notify('RTD/IR ALARM TRIPPED.')
        flag = True
    elif ezca['INTRLK_FLOW_ALRM'] != 0:
        log('FLOW ALARM TRIPPED.')
        notify('FLOW ALARM TRIPPED.')
        flag = True
    return flag


def get_tcs_power():
    """Calculate the power for the CO2 laser based on the PSL
    input power. Should be a linear relationship.
    """
    psl_input = ezca[':IMC-PWR_IN_OUTMON']
    rtn = np.interp(psl_input, CEN_CO2_POWER[:, 0], CEN_CO2_POWER[:, 1])
    return rtn


def is_co2_out_of_range():
    """Check that the current CO2 power is within a range of request power
    that is acceptable.
    CO2 power is calibrated to central masks so an extra factor and more tolerance
    has been added if annulus mask is inserted.
    """
    if ezca[':GRD-{}_STATE'.format(_SYSTEM_)] == 'NO_OUTPUT':
        nom_now = 0.0
    elif nodes[mask_node] == 'ANNULUS':
        nom_now = nom_annular_pwr  # used for ANNULUS
    else:
        nom_now = get_tcs_power()  # used for CENTRAL
    cur_co2 = ezca['LSRPWR_MTR_OUTPUT']
    if nodes[mask_node] == 'ANNULUS':
        if arm == 'X':
            return abs(nom_now - cur_co2) > CO2_RANGE * 2.0
        elif arm == 'Y':
            return abs(nom_now - cur_co2) > CO2_RANGE * 2.0
    else:
        return abs(nom_now - cur_co2) > CO2_RANGE


######

class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_fault():
            return 'FAULT'


##################################################
# States
##################################################


def gen_adjust_power(power_req,bootstrap=False):
    """State generator for a power adjusting state.
    If the Rotation Stage is busy from a prevous request, it will wait until 
    it is finished to request the new power. 
    """

    class ADJ_PWR(GuardState):
        @fault_checker
        def main(self):
            self.request_given_flag = False
            self.bootstrap_arg = bootstrap
            self.bootstrap = False
            self.initial_done = False
            self.timer['adjust power'] = 0

        @fault_checker
        def run(self):
            # Check if already has old request so busy
            if not is_busy() and self.request_given_flag == False and not self.bootstrap:
                log('Requesting {}W CO2 input power...'.format(power_req))
                time.sleep(0.5)
                ezca['LASERPOWER_POWER_REQUEST'] = power_req

                # Needs this otherwise the button is pressed before the power is in
                time.sleep(0.5)
                # Press the 'GO_TO_POWER' button
                ezca['LASERPOWER_COMMAND'] = 2
                # This just keeps it from running to the next state before it has
                # registered the command
                self.timer['adjust power'] = 2
                self.request_given_flag = True
            elif not is_busy() and self.timer['adjust power'] \
                    and self.request_given_flag == True and not self.bootstrap\
                    and self.bootstrap_arg:
                if not self.initial_done:
                    self.timer['pause_between_cmds'] = 2
                    self.initial_done = True
                elif self.timer['pause_between_cmds'] and self.initial_done:
                    log('RS done at power: {}. Bootstrapping.'.format(ezca['LSRPWR_MTR_OUTPUT']))
                    # NOT using the rs_bootstrap because the defined chans for TCS dont work with prefixes
                    # rsp.rs_bootstrap_power(f'TCS{arm}', power_req)
                    power_current = ezca['LSRPWR_MTR_OUTPUT']
                    power_adjusted = (power_req ** 2) / power_current
                    ezca['LASERPOWER_POWER_REQUEST'] = power_adjusted
                    ezca['LASERPOWER_COMMAND'] = 2

                    self.bootstrap = True

            elif not is_busy() and self.timer['adjust power'] \
                    and self.request_given_flag == True:
                if not self.bootstrap_arg:
                    return True
                elif self.bootstrap_arg and self.bootstrap:
                    log('At power: {}W.'.format(ezca['LSRPWR_MTR_OUTPUT']))
                    return True

    ADJ_PWR.request = False
    return ADJ_PWR


def gen_idle_state(index, requestable=True):
    class IDLE(GuardState):
        request = requestable

        @fault_checker
        def main(self):
            return True

        @fault_checker
        def run(self):
            if is_co2_out_of_range():
                notify("CO2 power is out of its expected range.")
            return True

    IDLE.index = index
    return IDLE


def gen_switch_masks(mask):
    """State generator for switching masks by requesting subbordinate
    node to mask choice.

    mask - 'central' or 'annulus'
    """

    class SWITCH_MASKS(GuardState):
        @fault_checker
        def main(self):
            nodes[mask_node] = mask.upper()

        @fault_checker
        def run(self):
            if nodes.completed:
                return True
            else:
                return False

    SWITCH_MASKS.request = False
    return SWITCH_MASKS


#################

# Basically a DOWN state with a more descriptive name
NO_OUTPUT = gen_idle_state(3)

TURNING_RS_TO_ZERO = gen_adjust_power(0)
TURNING_RS_TO_ZERO.goto = True


class INIT(GuardState):
    request = True

    def main(self):
        nodes.set_managed()
        if in_fault():
            return 'FAULT'
        # FIXME: I feel like there should be more here...
        else:
            if nodes[mask_node] == 'CENTRAL':
                return 'MASK_CENTRAL'
            elif nodes[mask_node] == 'ANNULUS':
                return 'MASK_ANNULUS'
            else:
                # b/c we are in some wacky state
                return 'NO_OUTPUT'


class FAULT(GuardState):
    index = 1
    redirect = False

    def run(self):
        if not in_fault():
            return True


class SEARCHING_FOR_HOME(GuardState):
    """Search for home in case the RS has drifted.

    """
    goto = True
    request = False

    @fault_checker
    def main(self):
        log('Rot. stage moving to home')
        # Give the search for home command
        # medm seems to say 1, but the IFO RS says 4? 4 seems to work
        ezca['LASERPOWER_COMMAND'] = 4
        # Use the timer to make sure that the node didn't run through this state.
        # It seems like the RS can take a bit of time before it actually executes the command
        self.timer['searching'] = 5

    # @status_check
    def run(self):
        # 0 = idle
        if is_busy():
            return False
        elif self.timer['searching']:
            return True


HOME = gen_idle_state(2)


####################
# Central heating states

# Main class to adjust power
class ADJUST_CENTRAL_PWR(GuardState):
    # goto = True
    request = False

    @fault_checker
    def main(self):
        power_req = get_tcs_power()
        log('Requesting {}W CO2 input power...'.format(power_req))
        ezca['LASERPOWER_POWER_REQUEST'] = power_req

        # Needs this otherwise the button is pressed before the power is in
        time.sleep(0.5)
        # Press the 'GO_TO_POWER' button
        ezca['LASERPOWER_COMMAND'] = 2
        # This just keeps it from running to the next state before it has
        # registered the command. 2sec is shorter than its moving time.
        self.timer['adjust power'] = 2

    @fault_checker
    def run(self):
        if not is_busy() and self.timer['adjust power']:
            if is_co2_out_of_range():
                return 'ADJUST_CENTRAL_PWR'
            else:
                return True
        else:
            return False


class PRE_HEATING(GuardState):
    """Idle state that can go to ADJUST_CENTRAL_PWR
    if the IFO power is changed. Uses a range  window
    so that the CO2 power isnt adjusting power constantly.
    """
    index = 6

    @fault_checker
    def run(self):
        # Check if the current CO2 power is within
        # some range of where it should be.
        isc_lock_state = ezca[':GRD-ISC_LOCK_STATE_N']
        # Don't adjust CO2 powers whilst in nominal low noise
        if is_co2_out_of_range() and isc_lock_state < 600:
            return 'ADJUST_CENTRAL_PWR'
        else:
            return True


HOLD_CENTRAL_POWER = gen_idle_state(7)

NOM_CENTRAL_POWER = gen_idle_state(8)

MASK_CENTRAL = gen_switch_masks('central')
MASK_CENTRAL.index = 4

####################
# Annular heating states

MASK_ANNULUS = gen_switch_masks('annulus')
MASK_ANNULUS.index = 5
# For now just read off the value we need to be at in lscparams.py
# Later, we could make this smarter or add a state for higher/lower power
GOTO_NOM_ANNULAR_PWR = gen_adjust_power(nom_annular_pwr, bootstrap=True)

NOM_ANNULAR_POWER = gen_idle_state(10)

##################################################
# Edges
##################################################

edges = [('ADJUST_CENTRAL_PWR', 'PRE_HEATING'),
         ('PRE_HEATING', 'NOM_CENTRAL_POWER'),
         ('NOM_CENTRAL_POWER', 'PRE_HEATING'),
         ('PRE_HEATING', 'HOLD_CENTRAL_POWER'),
         ('HOLD_CENTRAL_POWER', 'PRE_HEATING'),
         ('NO_OUTPUT', 'MASK_CENTRAL'),
         ('MASK_CENTRAL', 'ADJUST_CENTRAL_PWR'),
         ('SEARCHING_FOR_HOME', 'HOME'),
         ('TURNING_RS_TO_ZERO', 'NO_OUTPUT'),
         ('NO_OUTPUT', 'MASK_ANNULUS'),
         ('MASK_ANNULUS', 'GOTO_NOM_ANNULAR_PWR'),
         ('GOTO_NOM_ANNULAR_PWR', 'NOM_ANNULAR_POWER'),
         ]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
